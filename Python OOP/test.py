


# EX 1 & 2
class Fraction:
    def __init__(self, nr, dr=1):
        self.nr = nr
        self.dr = abs(dr)
        self._reduce()

    def show(self):
        print(f"{self.nr}/{self.dr}")

    def __str__(self):
        return f"{self.nr}/{self.dr}"

    def __repr__(self):
        return f"Fraction({self.nr}, {self.dr})"

    def __mul__(self, other):
        if isinstance(other,int):
            other = Fraction(other)
        mnr = self.nr * other.nr
        mdr = self.dr * other.dr
        res = Fraction(mnr, mdr)
        res._reduce()
        return  res

    def __add__(self, other):
        if isinstance(other,int):
            other = Fraction(other)
        anr = self.nr * other.dr + other.nr * self.dr
        adr = self.dr * other.dr
        res = Fraction(anr, adr)
        res._reduce()
        return res

    def __radd__(self):
        return self.__add__(other)

    def __sub__(self, other):
        if isinstance(other, int):
            other = Fraction(other)
        snr = self.nr * other.dr - other.nr * self.dr
        sdr = self.dr * other.dr
        res = Fraction(snr, sdr)
        res._reduce()
        return res
    
    def __eq__(self, other):
        return (self.nr*other.dr)==(self.dr*other.nr)
    
    def __lt__(self, other):
        return (self.nr*other.dr)<(self.dr*other.nr)
    
    def __le__(self, other):
        return (self.nr*other.dr)<=(self.dr*other.nr)
    
    @staticmethod
    def hcf(x,y):
        x, y = abs(x), abs(y)
        smaller = y if x>y else x
        s = smaller

        while s>0:
            if x%s==0 and y%s==0:
                break
            s -= 1
        return s

    def _reduce(self):
        h = Fraction.hcf(self.nr, self.dr)
        print(h)
        if h == 0:
            return

        self.nr 
        self.dr

f1 = Fraction(2, 3)
f1.show()
f2 = Fraction(4, 6)
f2.show()
print(f1==f2)
