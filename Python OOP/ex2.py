
import math

class BankAcc:
    def __init__(self, name, balance=0):
        # magic methods
        self.name = name
        self.balance = balance

    def display(self):
        print(f"Name: {self.name}")
        print(f"Balance: {self.balance}")

    def withdraw(self, amount):
        self.balance = self.balance - amount

    def deposit(self, amount):
        self.balance = self.balance + amount


class Book:
    def __init__(self, isbn, title, author, publisher, pages, price, copies):
        self.isbn = isbn
        self.title = title
        self.author = author
        self.publisher = publisher
        self.pages = pages
        self.price = price
        self.copies = copies

    def display(self):
        print(f"isbn: {self.isbn}")
        print(f"title: {self.title}")
        print(f"price: {self.price}")
        print(f"Number of copies: {self.copies}\n\n")


    @property
    def in_stock(self):
        if self.copies>0:
            return True
        return False
    
    def sell(self):
        if in_stock:
            self.copies -= 1
        else:
            print("The book is out of stock")

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, n_price):
        if  (50 <= n_price <= 1000):
            self._price = n_price
        else:
            raise ValueError("Price out of range")

class Fraction:
    def __init__(self, nr, dr=1):
        self.nr = nr
        self.dr = abs(dr)

    def show(self):
        print(f"{self.nr}/{self.dr}")

    def multiply(self, other):
        mnr = self.nr * other.nr
        mdr = self.dr * other.dr
        return Fraction(mnr, mdr)

    def add(self, other):
        anr = (self.nr * other.dr + other.nr * self.dr)
        adr = self.dr * other.dr
        return Fraction(anr, adr)


class Product():
    def __init__(self, id, marked_price, discount):
        self.id = id
        self.marked_price = marked_price
        self.discount = discount
    
    def display(self):
        print(self.id,  self.marked_price,  self.discount)

    @property
    def selling_price(self):
        d_price = 0.01*self.marked_price*self.discount
        selling_p = self.marked_price-d_price
        return selling_p

    @property
    def discount(self):
        if self.marked_price>500:
            return self._discount+2
        else:
            return self._discount

    @discount.setter
    def discount(self, a_d):
        self._discount = a_d

class Circle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return math.pi*self.radius**2

    @property
    def circumference(self):
        return 2*math.pi*self._radius

    @property
    def diameter(self):
        return self._radius*2

    @property
    def radius(self):
        return self._radius

    @radius.setter
    def radius(self, val):
        if val >= 0:
            self._radius = val
        else:
            raise ValueError("Radius cannot be negative.")

    
p1 = Product('X879', 400, 6)
p2 = Product('A234', 100, 5)
p3 = Product('B987', 990, 4)
p4 = Product('H456', 800, 6)

print(p1.id, p1.selling_price)
print(p2.id, p2.selling_price)
print(p3.id, p3.selling_price)
print(p4.id, p4.selling_price)
 
p4.discount = 10
print(p4.id, p4.selling_price) 
        

book1 = Book('957-4-36-547417-1', 'Learn Physics','Stephen', 'CBC', 350, 200,10)
book2 = Book('652-6-86-748413-3', 'Learn Chemistry','Jack', 'CBC', 400, 220,20)
book3 = Book('957-7-39-347216-2', 'Learn Maths','John', 'XYZ', 500, 300,5)
book4 = Book('957-7-39-347216-2', 'Learn Biology','Jack', 'XYZ', 400, 200,6)

books = [book1, book2, book3, book4]

for book in books:
    book.display()

jacks_books = [book for book in books if book.author=="Jack"]
for book in jacks_books:
    print(book.title)

f1 = Fraction(2,3)
f1.show()
f2 = Fraction(3,4)
f2.show()
f3 = f1.multiply(f2)
f3.show()
f4 = Fraction(5)
f5 = f1.multiply(f4)
f5.show()
f6 = f1.add(f2)
f6.show()


c1 = Circle(2)
print(c1.area())
print( c1.radius, c1.diameter, c1.circumference, c1.area() )