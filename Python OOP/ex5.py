
# EX 1
class Course:

    def __init__(self,  title, instructor, price, lectures):
        self.title = title
        self.instructor = instructor
        self.price = price
        self.lectures = lectures
        self.users = list()
        self.ratings = 0
        self.avg_rating = 0

    def __str__(self):
        return f"{self.title} by {self.instructor}"

    def new_user_enrolled(self, user):
        self.users.append(user)

    def received_a_rating(self, rating):
        self.avg_rating=(self.avg_rating * self.ratings + rating)/(self.ratings + 1)
        self.ratings += 1

    def show_details(self):
        print('Course Title : ', self.title)
        print('Intructor : ', self.instructor)
        print('Price : ', self.price)
        print('Number of Lectures : ', self.lectures)
        print('Users : ', self.users)
        print('Average rating : ', self.avg_rating)

class VideoCourse(Course):
    def __init__(self,  title, instructor, price, lectures, length_video):
        super().__init__(title, instructor, price, lectures)
        self.length_video=length_video

    def show_details(self):
        super().show_details()
        print(f"Video Length: {self.length_video}")


class PdfCourse(Course):
    def __init__(self, title, instructor, price, lectures, pages):
        super().__init__(title, instructor, price, lectures)
        self.pages=pages
    
    def show_details(self):
        super().show_details()
        print(f"Number of pages: {self.pages}")
    

bar = PdfCourse("Power systems", "Barry", 500, 14, 50)
bar.new_user_enrolled("jj")
bar.received_a_rating(5)
bar.show_details()

# EX 2
class Mother:
        def cook(self):
           print('Can cook pasta')
 
class Father:
        def cook(self):
             print('Can cook noodles')
 
class Daughter(Father, Mother):
          pass
 
class Son(Mother, Father):
         def cook(self):
             super().cook()
             print('Can cook butter chicken') 
 
d = Daughter()  
s = Son()
 
d.cook()
print()
s.cook()

# EX 3
class Person:
    def greet(self):
        print('I am a Person')
 
class Teacher(Person):
    def greet(self):
        Person.greet(self)    
        print('I am a Teacher')
 
class Student(Person):
    def greet(self):
        Person.greet(self)    
        print('I am a Student')
 
class TeachingAssistant(Student, Teacher):
     def greet(self):
         super().greet()
         print('I am a Teaching Assistant')
       
x = TeachingAssistant()
x.greet()

# EX 4

class Person:
    def __init__(self,id):
        self.id = id
        
class Teacher(Person):
    def __init__(self,id):
        super().__init__(id)
        self.id += 'T'
    
class Student(Person):
    def __init__(self,id):
        super().__init__(id)
        self.id += 'S'
   
class TeachingAssistant(Student, Teacher):
     def __init__(self,id):
        super().__init__(id)
       
x = TeachingAssistant('2675')
print(x.id)
y = Student('4567')
print(y.id)
z = Teacher('3421')
print(z.id)
p = Person('5749')
print(p.id)