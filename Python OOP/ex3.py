
# EX 1 & 2
class Fraction:
    def __init__(self, nr, dr=1):
        self.nr = nr
        self.dr = abs(dr)
        self._reduce()

    def show(self):
        print(f"{self.nr}/{self.dr}")

    def __mul__(self, other):
        if isinstance(other,int):
            other = Fraction(other)
        mnr = self.nr * other.nr
        mdr = self.dr * other.dr
        res = Fraction(mnr, mdr)
        res._reduce()
        return  res

    def __add__(self, other):
        if isinstance(other,int):
            other = Fraction(other)
        anr = self.nr * other.dr + other.nr * self.dr
        adr = self.dr * other.dr
        res = Fraction(anr, adr)
        res._reduce()
        return res
    def __sub__t(self, other):
        if isinstance(other, int):
            other = Fraction(other)
        snr = self.nr * other.dr - other.nr * self.dr
        sdr = self.dr * other.dr
        res = Fraction(snr, sdr)
        res._reduce()
    
    @staticmethod
    def hcf(x,y):
        x, y = abs(x), abs(y)
        smaller = y if x>y else x
        s = smaller

        while s>0:
            if x%s==0 and y%s==0:
                break
            s -= 1
        return s

    def _reduce(self):
        h = Fraction.hcf(self.nr, self.dr)
        if h == 0:
            return

        self.nr
        self.dr

f1 = Fraction(6, 36)
f1.show()
f2 = Fraction(2, -12)
f2.show()
f3 = f1.multiply(f2)
f3.show()
f4 = f1.add(5)
f4.show()
f5 = f1.multiply(5)
f5.show()

# EX 3
class SalesPerson():
    total_revenue = 0
    names = list()

    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.sales_amount = 0
        SalesPerson.names.append(name)

    def make_sale(self, money):
        self.sales_amount += money
        SalesPerson.total_revenue += money

    def show(self):
        print(self.name, self.age, self.sales_amount)

s1 = SalesPerson('Bob', 25)
s2 = SalesPerson('Ted', 22)
s3 = SalesPerson('Jack', 27)
 
s1.make_sale(1000)
s1.make_sale(1200)
s2.make_sale(5000)
s3.make_sale(3000)
s3.make_sale(8000)
 
s1.show()
s2.show()
s3.show()

print(SalesPerson.total_revenue)
print(SalesPerson.names)

# EX 4 & 5

class Employee:
    domains = set()
    allowed_domains = {'yahoo.com', 'gmail.com', 'outlook.com'}

    def __init__(self, name, email):
        self.name = name
        self.email = email
        Employee.domains.add(email.split('@')[-1])
    
    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, new_email):
        if new_email.split('@')[-1] in Employee.allowed_domains:
            self._email = new_email

        else:
            raise RuntimeError("Domain name not allowed")

    def display(self):
        print(self.name, self.email)

    
e1 = Employee('John','john@gmail.com')
e2 = Employee('Jack','jack@yahoo.com')
e3 = Employee('Jill','jill@outlook.com')
e4 = Employee('Ted','ted@yahoo.com')
# e5 = Employee('Tim','tim@xmail.com')
e6 = Employee('Mike','mike@yahoo.com')

print(Employee.domains)

# EX 6
class Stack:
    MAX_SIZE = 3

    def __init__(self):
        self.items = list()

    def is_empty(self):
        return self.items == []

    def size(self):
        return len(self.items)

    def push(self, item):
        if len(self.items) < Stack.MAX_SIZE:
            self.items.append(item)
        else:
            raise RuntimeError("Stack Overflow")
    
    def pop(self):
        if self.is_empty():
            raise RuntimeError("Stack is empty")
        return self.items.pop()

    def peek(self):
        if not self.is_empty():
            return self.items[-1]
        else:
            raise RuntimeError("Stack is empty")

    def display(self):
        print(self.items)


# if __name__=="__main__":
#     st = Stack()

#     while True:
#         print("1.Push") 
#         print("2.Pop") 
#         print("3.Peek") 
#         print("4.Size")
#         print("5.Display") 
#         print("6.Quit")

#         choice = int(input("Enter your choice : "))
 
#         if choice == 1:
#             x=int(input("Enter the element to be pushed : "))
#             st.push(x) 
#         elif choice == 2:
#             x=st.pop() 
#             print("Popped element is : " , x) 
#         elif choice == 3:
#             print("Element at the top is : " , st.peek()) 
#         elif choice == 4:
#             print("Size of stack " , st.size()) 
#         elif choice == 5:
#             st.display()         
#         elif choice == 6:
#             break
#         else:
#             print("Wrong choice") 
#         print() 

# EX 7

class BankAcc:
    bank_name = "Access"

    def __init__(self, name, balance=0, bank=bank_name):
        # magic methods
        self.name = name
        self.balance = balance
        self.bank = bank

    def display(self):
        print(f"Name: {self.name}")
        print(f"Balance: {self.balance}")
        print(f"Bank: {self.bank}")

    def withdraw(self, amount):
        self.balance = self.balance - amount

    def deposit(self, amount):
        self.balance = self.balance + amount


a1 = BankAcc('Mike', 200, "GTC")
a2 = BankAcc('Tom')
 
a1.display()
a2.display()

