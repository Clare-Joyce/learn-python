
# define a class
class Person:

    def set_details(self, name, age):
        self.name = name
        self.age = age
    
    def display(self):
        print("I am ", self.name)

    def greet(self):
        if self.age < 80:
            print("Hello, how are you doing?")
        else:
            print("Hello, how do you do?")
        self.display()

p1 = Person()
p2 = Person()

# p2.greet()
p1.set_details("bob", 200)
p1.greet()
print(p1.name)

class Test:
    def method1(self,x):
            self.x = x
    
    def method2(self):
            x += 10
    
    def display(self):
        print(self.x)
 
t = Test()
t.method1(5)
t.method2()
t.display()